# Estructuras de Control

## Condicional if

### if

````python
#...
a = 10;
if a==10:
    print("a es igual a 10")j

````
### if-else

````python
#...
a = 10;
if a==10:
    print("a es igual a 10")
else:
    print("a no es igual a 10")
````

### if-elif

````python
#...
a = 10;
if a==10:
    print("a es igual a 10")
elif a==5:
    print("a es igual a 5")
````

### if-elif-else

````python
#...
a = 10;
if a==10:
    print("a es igual a 10")
elif a==5:
    print("a es igual a 5")
else:
    print("a no es igual 5 ni a 10)
````

## Estructuras Iterativas

### While

````python
#...
a = 0;
while a<10:
    a+=1
    print(a)
````

#### While -else

````python
#...
a = 0;
while a<10:
    a+=1
    print(a)
else:
    print("fin del contador")
````


### For

=== "Lista"

    ````python
    #...
    animales = ["perro", "gato","pez","hamster"]
    for a in animales:
        print(a)
    ````

=== "String"

    ````python
    #...
    libro = "Sueñan los androides con ovejas electricas"
    for l in libro:
        print(l)
    ````


=== "Rango"

    ````python
    #...
    for a in range(5):
        print(a)
    ````


#### For-Else

    ````python
    #...
    for a in range(5):
        print(a)
    else:
        print("Fin del rango")
    ````