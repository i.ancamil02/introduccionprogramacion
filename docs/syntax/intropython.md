# Introducción a Python

## Ejecutar un programa

Es posible ejecutar código python desde la consola.

![consola](consola.png)

Tambien es posible ejecutar un archivo con código python 


=== "Ejecucion""
    ![file](file.png)

=== "file.py"
    ````python
    a=0
    while a<10:
        print(a)
        a+=1

    ````

## Indentación

Python utiliza la indentación para señalar un bloque de código, si no es utilizada de forma correcta lanzará errores al momento de ejecutar el programa.

=== "uso correcto"

    ````python
    a = 10
    if a==10:
        print("a es igual a 10")j

    ````

=== "uso incorrecto"

    ````python
    a = 10
    if a==10:
    print("a es igual a 10")j

    ````
=== "uso incorrecto"

    ````python
    a = 10
    if a==10:
      print("a es igual a 10")
        print("a= 10")
    ````


## Variables

Las variables se crean al momento de que se les asigne un valor

=== "Variable numérica"

    ````python
    a = 10
    numero = 17.0
    pi = 3.14
    ````

=== "Variable alfanimerica"

    ````python
    caracter = 'a'
    nombre = "Marcos"
    titulo = "El Ingenioso Hidalgo Don Quijote de la Mancha"
    ````

## Comentarios

Los comentarios nos permiten añadir notas, documentación, comentarios, etc.

````python
# comentario en python
# los comentarios no serán ejecutados

````